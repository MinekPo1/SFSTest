using HarmonyLib;
using ModLoader;
using ModLoader.Helpers;
using UnityEngine.Networking;

using Console = ModLoader.IO.Console;
using SharingRequester = SFS.Sharing.SharingRequester;
using RequestUtil = SFS.Sharing.RequestUtil;
using InitializationResult = SFS.Sharing.InitializationResult;

namespace SFSTest {
	public class Main : Mod {

		public override string ModNameID => "MinekPo1.SFSTest";
		public override string DisplayName => "sfs test";
		public override string Author => "MinekPo1";
		public override string MinimumGameVersionNecessary => "1.5.8.5";
		public override string ModVersion => "v0.1.0";
		public override string Description => "test";

		private RequestUtil reqUtil = new RequestUtil();
		private SharingRequester requester = new SharingRequester();

		// This initializes the patcher. This is required if you use any Harmony patches.
		public static Harmony patcher;

		public override void Load() {
			// This tells the loader what to run when your mod is loaded.
			Console.commands.Add(CommandHelp);
			Console.commands.Add(CommandSharingGetData);
			Console.commands.Add(CommandSharingGetRequest);
			Console.commands.Add(CommandSharingPostRequest);

			SFSTest.DevSettingsPatch.ConsoleCommandManager.Init();

			reqUtil.Initialize();
			
			requester.Initialize(delegate(InitializationResult result) {
				if (result != InitializationResult.Success) {
					Console.main.WriteText("Failed to initialize the requester: " + (result == InitializationResult.SocialFailed ? "Social Failed" : "Server Failed" ));
				}
				else {
					if (string.IsNullOrEmpty(requester.loginToken)) {
						Console.main.WriteText("Token is empty or null");
					}
					reqUtil.SetToken(requester.loginToken);
				}
			});
			
		}

		private bool CommandHelp(string s) {
			if (s.StartsWith("SFSTest:")) {
				s = s.Substring(8);
			}
			if ( !(s == "help" || s.StartsWith("help ")) ) {
				return false;
			}
			bool printedDiscriminator = false;
			if (s == "help" || s == "help sharing") {
				if (!printedDiscriminator) {
					Console.main.WriteText("SFSTest:");
					printedDiscriminator = true;
				}
				Console.main.WriteText("  sharing:");
				Console.main.WriteText("    get sharingID and loginToken");
				Console.main.WriteText("  sharing get <apiPath>:");
				Console.main.WriteText("    send authorised get request");
				Console.main.WriteText("  sharing post <apiPath> <params>:");
				Console.main.WriteText("    send authorised post request. Params are in the format key=value");
			}
			if (s == "help" || s == "help sharing") {
				if (!printedDiscriminator) {
					Console.main.WriteText("SFSTest:");
					printedDiscriminator = true;
				}

				Console.main.WriteText("  dev$:");
				Console.main.WriteText("    show the current state of dev settings");
				Console.main.WriteText("  dev$ <setting>?:");
				Console.main.WriteText("    show the current state of a dev setting");
				Console.main.WriteText("  dev$ <setting>:");
				Console.main.WriteText("    enable a dev setting");
				Console.main.WriteText("  dev$ no<setting>:");
				Console.main.WriteText("    disable a dev setting");
			}
			return true;
		}

		private bool CommandSharingGetData(string s) {
			if (s.StartsWith("SFSTest:")) {
				s = s.Substring(8);
			}
			if (s != "sharing") {
				return false;
			}

			string userAgent = reqUtil.GetRequest("").GetRequestHeader("User-agent");

			Console.main.WriteText($"sharingId: {requester.sharingId}");
			Console.main.WriteText($"loginToken: {requester.loginToken}");
			Console.main.WriteText($"UA: {userAgent}");

			return true;
		}

		private bool CommandSharingGetRequest(string s) {
			if (s.StartsWith("SFSTest:")) {
				s = s.Substring(8);
			}
			if (!s.StartsWith("sharing get ")) {
				if (s == "sharing get") {
					return true;
				}
				return false;
			}

			string[] parts = s.Split(' ');

			UnityWebRequest request = reqUtil.AuthedGetRequest(parts[2]);

			reqUtil.SendRequest(request, delegate(long status, string val){
				Console.main.WriteText($"Response from {request.url}: {status}");
				Console.main.WriteText(val);
			});

			return true;
		}

		private bool CommandSharingPostRequest(string s) {
			if (s.StartsWith("SFSTest:")) {
				s = s.Substring(8);
			}
			if (!s.StartsWith("sharing post ")) {
				return false;
			}

			string[] parts = s.Split(' ');

			(string, object)[] data = new (string, object)[parts.Length - 3];
			for (int i = 3; i < parts.Length; i++ ) {
				string[] split = parts[i].Split('=');
				if (split.Length != 2) {
					Console.main.WriteText($"`{parts[i]}` is not in the format key=value");
					return true;
				}
				data[i-3] = (split[0],split[1]);
			}

			UnityWebRequest request = reqUtil.AuthedPostRequest(parts[2], data);

			reqUtil.SendRequest(request, delegate(long status, string val){
				Console.main.WriteText($"Response [{status}]");
				Console.main.WriteText(val);
			});

			return true;
		}

		public override void Early_Load() {
			// This method runs before anything from the game is loaded. This is where you should apply your patches, as shown below.

			// The patcher uses an ID formatted like a web domain.
			Main.patcher = new Harmony("pl.minekpo1.test");

			// This pulls your Harmony patches from everywhere in the namespace and applies them.
			Main.patcher.PatchAll();
		}
	}
}


