using HarmonyLib;
using UnityEngine;
using System;
using System.Text.RegularExpressions;

using static DevSettings;
using Console = ModLoader.IO.Console;

namespace SFSTest.DevSettingsPatch {
	public class ValueOverrides {
		// TODO: Support `DisableParts`
		// public static string[] DisableParts;
		public static bool DisableAstronauts = DevSettings.DisableAstronauts;
		public static bool DisableNewBuild = DevSettings.DisableNewBuild;
		public static bool HasNewParticles = DevSettings.HasNewParticles;
	}

	[HarmonyPatch(typeof(DevSettings))]
	[HarmonyPatch(nameof(DevSettings.DisableAstronauts), MethodType.Getter)]
	public class DisableAstronoutsPatch {
		public static bool Prefix(ref bool __result) {
			__result = ValueOverrides.DisableAstronauts;
			return true;
		}
	}
	
	[HarmonyPatch(typeof(DevSettings))]
	[HarmonyPatch(nameof(DevSettings.DisableNewBuild), MethodType.Getter)]
	public class DisableNewBuildPatch {
		public static bool Prefix(ref bool __result) {
			__result = ValueOverrides.DisableNewBuild;
			return true;
		}
	}
	
	[HarmonyPatch(typeof(DevSettings))]
	[HarmonyPatch(nameof(DevSettings.HasNewParticles), MethodType.Getter)]
	public class HasNewParticlesPatch {
		public static bool Prefix(ref bool __result) {
			__result = ValueOverrides.HasNewParticles;
			return true;
		}
	}

	public class ConsoleCommandManager {
		public static void Init () {
			Console.commands.Add(CommandShowDevSettings);
			Console.commands.Add(CommandSetBoolDevSetting);
		}

		public static bool CommandShowDevSettings (string s) {
			if (s.StartsWith("SFSTest:")) {
				s = s.Substring(8);
			}

			Match match = Regex.Match(s, "^dev\\$( ([A-Za-z]+)\\?)?$");
			if (!match.Success) {
				return false;
			}

			bool didPrintAnything = false;
			if (match.Groups[2].Value == "" || match.Groups[2].Value == "astronauts") {
				Console.main.WriteText(ValueOverrides.DisableAstronauts ? "noastronauts" : "astronauts");
				didPrintAnything = true;
			}
			if (match.Groups[2].Value == "" || match.Groups[2].Value == "newbuild") {
				Console.main.WriteText(ValueOverrides.DisableNewBuild ? "nonewbuild" : "newbuild");
				didPrintAnything = true;
			}
			if (match.Groups[2].Value == "" || match.Groups[2].Value == "newparticles") {
				Console.main.WriteText(ValueOverrides.HasNewParticles ? "newparticles" : "nonewparticles");
				didPrintAnything = true;
			}
			if (!didPrintAnything) {
				Console.main.WriteText("[!!] unknown dev setting found.");
			}
			return true;
		}

		public static bool CommandSetBoolDevSetting (string s) {
			if (s.StartsWith("SFSTest:")) {
				s = s.Substring(8);
			}

			Match match = Regex.Match(s, "^dev\\$ (no)?([A-Za-z]+)?$");
			if (!match.Success || match.Groups[2].Value == "") {
				return false;
			}
			
			if (match.Groups[2].Value == "astronauts") {
				ValueOverrides.DisableAstronauts = match.Groups[1].Value == "no";
				return true;
			}
			if (match.Groups[2].Value == "newbuild") {
				ValueOverrides.DisableNewBuild = match.Groups[1].Value == "no";
				return true;
			}
			if (match.Groups[2].Value == "astronouts") {
				ValueOverrides.HasNewParticles = match.Groups[1].Value == "";
				return true;
			}
			
			Console.main.WriteText("[!!] unknown dev setting found.");
			return true;

		}
	}
}
